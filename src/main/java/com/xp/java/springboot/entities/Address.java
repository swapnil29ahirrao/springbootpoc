package com.xp.java.springboot.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
//@Table(name = "Emp_Address")
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "addId")
public class Address {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int addId;
	
	private String addType;
	private String state;
	private String pincode;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn
	@JsonBackReference
	private Employee emp;
	
	public Address(int addId, String addType, String state, String pincode, Employee emp) {
		super();
		this.addId = addId;
		this.addType = addType;
		this.state = state;
		this.pincode = pincode;
		this.emp = emp;
	}
	public Employee getEmp() {
		return emp;
	}
	public void setEmp(Employee emp) {
		this.emp = emp;
	}
	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getAddId() {
		return addId;
	}
	public void setAddId(int addId) {
		this.addId = addId;
	}
	public String getAddType() {
		return addType;
	}
	public void setAddType(String addType) {
		this.addType = addType;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	@Override
	public String toString() {
		return "Address [addId=" + addId + ", addType=" + addType + ", state=" + state + ", pincode=" + pincode + "]";
	}
	
	

}
